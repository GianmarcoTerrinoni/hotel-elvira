<?php

$count = $_GET['count'];

if($count == -1){


$titoletto="Errore";
$esito="E-Mail non inviata. Riprovare";



}else{


  $titoletto="Success";
  $esito="E-Mail inviata con successo.";



}
?>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta charset="utf-8">
    <title>Hotel Elvira - Mail</title>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="true">
    <meta name="apple-touch-fullscreen" content="yes">
    <!-- FAVICON-->
    <link rel="apple-touch-icon" sizes="57x57" href="../favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../favicon/apple-touch-icon-180x180.png">
    <link rel="manifest" href="../favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="../favicon/mstile-144x144.png">
    <meta name="msapplication-config" content="../favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Google Fonts-->
    <link href="http://fonts.googleapis.com/css?family=Playfair+Display:400,400italic,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,400,700" rel="stylesheet" type="text/css">
    <!-- OWLSLIDER-->
    <link rel="stylesheet" href="../js/libs/owl.carousel.2.0.0-beta.2.4/css/owl.carousel.css" type="text/css" media="all" data-module="owlslider">
    <link rel="stylesheet" href="../js/libs/owl.carousel.2.0.0-beta.2.4/css/owl.theme.default.css" type="text/css" media="all" data-module="owlslider">
    <!-- ANIMATE.CSS LIBRARY-->
    <link rel="stylesheet" href="../css/libs/animate.min.css" type="text/css" media="all">
    <!-- ICON WEB FONTS-->
    <!-- HEADER SCRIPTS	-->
    <script type="text/javascript" src="../js/libs/modernizr.custom.48287.js"></script>
    <!-- MAIN STYLESHEETS-->
    <link rel="stylesheet" href="../css/theme_custom_bootstrap.min.css" type="text/css" media="all">
    <link rel="stylesheet" href="../css/style.css" type="text/css" media="all">
  </head>
  <body>

      <div class="main">
        <section id="home" style="background-image: url(../images/mail-back.jpg)" class="text-center double_padding transparent_film">
          <div class="container">
            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                <div class="caption caption_elegant">
                  <div class="inner">
                    <div class="t3 uppercase"><?php echo $titoletto;?></div>
                    <div class="t1" ><?php echo $esito;?></div><a href="../../index.html" class="margin_top_half btn btn-primary">TORNA ALLA HOME</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
  </body>
</html>
